const gulp = require('gulp')

const clean = require('./clean')
const serve = require('./serve')
const pug2html = require('./pug2html')
const styles = require('./styles')
const script = require('./script')


function defaultTask(cb) {
    // place code for your default task here
    cb();
}

exports.default = defaultTask

const dev = gulp.parallel(pug2html, styles, script)

module.exports.start = gulp.series(clean, dev, serve)