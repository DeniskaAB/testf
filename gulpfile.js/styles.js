const gulp = require('gulp')
const plumber = require('gulp-plumber')
const sass = require('gulp-sass')
const gulpStyleLint = require('gulp-stylelint')

module.exports = function styles() {
    return gulp.src('src/styles/*.scss')
        .pipe(plumber())
        .pipe(gulpStyleLint({
            failAfterError:false,
            reporters:[
                {
                    formatter: 'string',
                    console:true
                }
            ]
        }))
        .pipe(sass())
        .pipe(gulp.dest('build/css'))
}
